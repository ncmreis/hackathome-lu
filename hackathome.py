import numpy as np
import requests
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit 

class General:

	def __init__(self):
		self.func_parameters = None
		self.model_dataframe = None
		self.original_features = None
		self.original_labels = None
		self.prediction_curve = None
		# for the days we already know this should be the same as the 
		# model_dataframe

	@staticmethod
	def select_country(dataframe, country):
		""" Creates a dataframe to select your country. Important as it converts 
		the dates to days 
		"""
		df = dataframe.copy()
		df = df[df.country == country]
		df['days'] = (df.date - df.date.min()).dt.days
		return df

	def fit(self, X, y):
		"""
		X: A series with the days since the beginning of the first n confirmed case
		y: The confirmed cases 
		"""
		self.original_features = X.copy()
		self.original_labels = y.copy()

		x = np.array(X)
		y = np.array(y)

		self.func_parameters, _ = curve_fit(self.function, x, y, maxfev=500000)
			
		self.model_dataframe = pd.DataFrame(x, self.function(x, *self.func_parameters)).reset_index()
		# print("this is model dataframe", self.model_dataframe.columns)
		self.model_dataframe.columns = ['confirmed', 'days']
		# print("this is model dataframe", self.model_dataframe.columns)

		return self

	def visualize_curve(self, country_dataframe, predictions=False):
		""" 
		Plot the curve fited by fit onto the country dataframe.
		predictions: Boolean to indicate if you want to visualize the predictions
		"""
		if predictions: 
		  plt.plot(self.prediction_curve['days'], self.prediction_curve['confirmed'], c="blue", label="model")
		  plt.plot(country_dataframe.days, country_dataframe.confirmed, 'r+', label="original data")
		  
		else:
		  plt.plot(self.model_dataframe['days'], self.model_dataframe['confirmed'], c="blue", label="model")
		  plt.plot(country_dataframe.days, country_dataframe.confirmed, 'r+', label="original data")


		plt.xlabel('Number of days since the first case')
		plt.ylabel('Number of confirmed cases')

		plt.title('Evolution of confirmed cases since the 1st case')

		plt.legend()
		plt.show()

	def predict(self, number_days):
		""" Return a dataframe with the predictions for the specific country <number_days>
		ahead"""
		next_days = list(range(0, len(self.original_features) + number_days))
		dates = pd.DataFrame(next_days)
		dates.columns = ['days']

		all_days = np.array(dates['days'])

		# use the parameters learnt during the fit
		model_predictions = self.function(all_days, *self.func_parameters)

		self.prediction_curve = pd.DataFrame(all_days, model_predictions).reset_index()
		self.prediction_curve.columns = ["confirmed", "days"]
		return self.prediction_curve

#############################

class Exponential(General):

  @staticmethod
  def function(x, a, b, c, d):
     return a * np.exp(b * x + c) + d

#############################

class Linear(General):

  @staticmethod
  def function(x, m, b):
    return m * x + b

#############################

class Logistic(General):

  @staticmethod
  def function(x, a, b, c, d, e, f):
     return a + b * np.exp(c + d) / (1 + np.exp(e * x + f))

#############################

class Logistic_widget(General):

  @staticmethod
  def function(x, a, b, c, d):
     return a + b / (1 + np.exp(c * x + d))

#############################

class Gompertz(General):

  @staticmethod
  def function(x, a, b, c, d):
     return a * np.exp(-b * np.exp(c * x) + d)